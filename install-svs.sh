#!/bin/bash

#################################
#### Rubén Ignacio Couoh Ku. ####
#################################

{

SVS_USER=${SUDO_USER:-${USER:-naika}}
ROOT=${HOME:-/home/naika}
APP=svs

# GIT
GIT_USER=socialvideoscreen
GIT_APP=svs

# Repositorios locales.
INSTALL_DIR=$ROOT/.$APP
COPY_INSTALL_DIR=$ROOT/$APP

# Aplicaciones.
APP_SERVER=${COPY_INSTALL_DIR}/svs-server
APP_CONFIG=${COPY_INSTALL_DIR}/svs-config

# logs
NPMLOG=$ROOT/npm-logs.log

BACKGROUND_SYSTEM=/usr/share/images/desktop-base/splash.jpg

SVS_CONFIG_DIR=$ROOT/.svsconfig
THEME=/usr/share/plymouth/themes/joy
BACKGROUND=$SVS_CONFIG_DIR/background
SPLASH=$INSTALL_DIR/splash.png
RSA_DIR=$SVS_CONFIG_DIR/.rsa

NETWORK_INTERFACES=/etc/network/interfaces
AUTORUN=$SVS_CONFIG_DIR/svs_init.sh
AUTOSTART=/etc/xdg/autostart/svs.desktop

SERVER=149.56.24.80
PORT=22
SERVER_USER=socialscreen
REMOTE_RSA_DIR=/home/socialscreen/svs-server/resources/keys

### Tunel SSH ###
PERSISTENCE=$INSTALL_DIR/svs-scripts/persistence.sh
CRONTAB=${SVS_CONFIG_DIR}/.tunnel-ssh

DROP_CACHES_FILE=$INSTALL_DIR/svs-scripts/drop-caches.sh
#
CRONTAB_AUTO_RELOAD=${SVS_CONFIG_DIR}/.auto-reload
#
cd $ROOT

svs_print_red() {

  local RED
  RED='\033[0;31m'
  svs_print $RED$*
}

svs_print_green() {

  local GREEN
  GREEN='\033[0;32m'
  svs_print $GREEN$*
}

svs_print_orange() {

  local ORANGE
  ORANGE='\033[0;33m'
  svs_print $ORANGE$*
}

svs_print_blue() {

  local GREEN
  GREEN='\033[0;34m'
  svs_print $GREEN$*
}

svs_print_red() {

  local RED
  RED='\033[0;31m'
  svs_print $RED$*
}

svs_print() {

  local NC
  local MESSAGE

  MESSAGE=$*
  NC='\033[0m'

  echo -e $MESSAGE$NC
}

svs_default_vars() {

  svs_print_green "[*] Ingrese los datos de instalación.\n"
  # Usuario local
  local _USER
  read -p "Usuario SVS: (${SVS_USER}) < " _USER </dev/tty
  SVS_USER=${_USER:-$SVS_USER}
  svs_print_blue "[*] Usuario SVS: ${SVS_USER}"

  # Home
  local _HOME
  read -p "Home: (${ROOT}) < " _HOME </dev/tty
  ROOT=${_HOME:-$ROOT}
  svs_print_blue "[*] Home: ${ROOT}"

  # Directorio de a aplicación.
  local _APP
  read -p "Carpeta de instalación: (${APP}) < " _APP </dev/tty
  APP=${_APP:-$APP}
  svs_print_blue "[*] Carpeta de instalación: ${APP}"

  # Usuario Git.
  local _GIT_USER
  read -p "Usuario GIT: (${GIT_USER}) < " _GIT_USER </dev/tty
  GIT_USER=${_GIT_USER:-$GIT_USER}
  svs_print_blue "[*] Usuario GIT: ${GIT_USER}"

  # Repositorio git.
  local _GIT_APP
  read -p "Repositorio GIT: (${GIT_APP}) < " _GIT_APP </dev/tty
  GIT_APP=${_GIT_APP:-$GIT_APP}
  svs_print_blue "[*] Repositorio GIT: ${GIT_APP}"

  # Servidor.
  local _SERVER
  read -p "Servidor: (${SERVER}) < " _SERVER </dev/tty
  SERVER=${_SERVER:-$SERVER}
  svs_print_blue "[*] Servidor: ${SERVER}"

  # Puerto SSH.
  local _PORT
  read -p "Puerto SSH: (${PORT}) < " _PORT </dev/tty
  PORT=${_PORT:-$PORT}
  svs_print_blue "[*] Puerto SSH: ${PORT}"

  # Usuario del servidor.
  local _SERVER_USER
  read -p "Usuario del servidor: (${SERVER_USER}) < " _SERVER_USER </dev/tty
  SERVER_USER=${_SERVER_USER:-$SERVER_USER}
  svs_print_blue "[*] Usuario del servidor: ${SERVER_USER}"

  # Directorio RSA remoto.
  local _REMOTE_RSA_DIR
  read -p "Carpeta remota RSA: (${REMOTE_RSA_DIR}) < " _REMOTE_RSA_DIR </dev/tty
  REMOTE_RSA_DIR=${_REMOTE_RSA_DIR:-$REMOTE_RSA_DIR}
  svs_print_blue "[*] Carpeta remota RSA: ${REMOTE_RSA_DIR}"
}

svs_source() {

  local SVS_SOURCE_URL
  SVS_SOURCE_URL="git@bitbucket.org:${GIT_USER}/${GIT_APP}.git"
  echo "${SVS_SOURCE_URL}"
}

svs_branch() {

  echo "master"
}

svs_generate_rsa_git() {

  local UUID
  UUID=$(sudo dmidecode | grep UUID | cut -d ' ' -f 2)

  svs_print_green "[+] Generando llaves RSA para conectarse a Bitbucket."
  command sudo ssh-keygen -b 2048 -C "${UUID}@root.device.com" <> /dev/tty

  local ID_RSA_PUB
  ID_RSA_PUB=$(cat /root/.ssh/id_rsa.pub)

  svs_print_orange "[*] Agrege las credenciales RSA a https://bitbucket.org/account/signin antes de continuar."
  svs_print_blue "[*] Usuario: social.video.screen.device@gmail.com"
  svs_print_blue "[*] RSA:\n${ID_RSA_PUB}"

  svs_print_green "[*] Despues de agregar las llaves a Bitbucket."
  svs_print_red "[*] Presione [enter] para continuar..."
  read yn </dev/tty
}

svs_init() {

  svs_print_green "[+] Creando ${SVS_CONFIG_DIR}"
  command install -o "${SVS_USER}" -g "${SVS_USER}" -d "${SVS_CONFIG_DIR}"

  if [ ! -f $UPDATED ]; then
    echo 1 > $UPDATED
  fi

  if [ ! -f $LASTUPDATE ]; then
    DATE=$(git show -s --format=%ci)
    echo $DATE > $LASTUPDATE
  fi
}

svs_install() {

  #
  if [ -d "${INSTALL_DIR}" ]; then
    svs_print_orange "[-] Eliminando ${INSTALL_DIR}."
    command rm -rf "$INSTALL_DIR" || svs_print_red "[-] No se pudo eliminar ${INSTALL_DIR}."
  fi

  mkdir -p "${INSTALL_DIR}"

  svs_print_green "[+] Clonando ${GIT_APP} en ${INSTALL_DIR}."
  command sudo git clone -b "$(svs_branch)" "$(svs_source)" "${INSTALL_DIR}" || {
    echo >&2 '\033[0;31mError al clonar el repositorio.\033[0m'
    exit 2;
  }

  svs_print_green "[+] Creando copia en ${COPY_INSTALL_DIR}."
  if [ -d "${COPY_INSTALL_DIR}" ]; then
    command rm -rf "${COPY_INSTALL_DIR}"
  fi

  command cp -rf "$INSTALL_DIR" "$COPY_INSTALL_DIR"
  command chown -R "${SVS_USER}:${SVS_USER}" "$INSTALL_DIR"
  command chown -R "${SVS_USER}:${SVS_USER}" "$COPY_INSTALL_DIR"
}

svs_npm_install_deps() {

  svs_print_green "[+] Instalando dependencias de ${APP_SERVER}"
  command runuser -l "${SVS_USER}" -c "cd ${APP_SERVER} && npm install > ${NPMLOG}"

  svs_print_green "[+] Instalando dependencias de ${APP_CONFIG}"
  command runuser -l "${SVS_USER}" -c "cd ${APP_CONFIG} && npm install >> ${NPMLOG}"
}

svs_generate_keys_rsa () {

  local UUID
  svs_print_green "[+] Creando ${RSA_DIR}."
  command install -o "${SVS_USER}" -g "${SVS_USER}" -d "${RSA_DIR}"

  svs_print_green "[+] Generando llaves RSA."
  UUID="$(dmidecode -t 1 | grep UUID | sed 's/.*UUID://;s/ //g' | tr [:upper:] [:lower:])"
  command openssl genrsa -out "${RSA_DIR}/.priv.pem" 2048
  command openssl rsa -pubout -in "${RSA_DIR}/.priv.pem" -out "${RSA_DIR}/.pub.pem"

  svs_print_green "[+] Subiendo llaves RSA a ${SERVER_USER}@${SERVER}:${REMOTE_RSA_DIR}"
  command scp -P $PORT "${RSA_DIR}/.pub.pem" "${SERVER_USER}@${SERVER}:${REMOTE_RSA_DIR}/${UUID}.pem" || {
    local RSA
    RSA=$(cat "${RSA_DIR}/.pub.pem")
    svs_print_red "[*] Fallo al agregar la llave RSA en ${SERVER_USER}@${SERVER}:${REMOTE_RSA_DIR}"
    svs_print_red "[*] Intentelo manualmente."
    svs_print_blue "${RSA_DIR}/.pub.pem\n${RSA}"
  }
}

svs_set_background_splash() {

  svs_print_green "[+] Configurando tema (joy)."
  command sudo /usr/sbin/plymouth-set-default-theme --list
  command sudo /usr/sbin/plymouth-set-default-theme joy

  svs_print_green "[+] Copiando background.jpg"
  if [ -f "$INSTALL_DIR/background.jpg" ]; then
    command cp "${INSTALL_DIR}/background.jpg" "${BACKGROUND_SYSTEM}"
  fi

  svs_print_green "[+] Creando enlace simbolico del background."
  if [ ! -L "${BACKGROUND}" ]; then
    command ln -s "${BACKGROUND_SYSTEM}" "${BACKGROUND}"
  fi

  svs_print_green "[+] Copiando splash.png."
  if [ -f "${INSTALL_DIR}/splash.png" ]; then
    command cp "${INSTALL_DIR}/splash.png" "${THEME}/background.png"
    command cp "${INSTALL_DIR}/splash.png" "${THEME}/backgroundpng"
  fi

  svs_print_green "[+] Ejecutando update-initramfs."
  command sudo update-initramfs -u -k "$(uname -r)"
}

svs_configure_network_interface () {

svs_print_green "[+] Generando ${NETWORK_INTERFACES}"

cat << EOF > $NETWORK_INTERFACES
auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet dhcp

auto eth0:0
allow-hotplug eth0:0
iface eth0:0 inet static
  address 10.0.0.1
  netmask 255.0.0.0
EOF
}

svs_generate_autorun () {

svs_print_green "[+] Generando ${AUTORUN}."
cat << EOF > $AUTORUN
#!/bin/bash
if [ -f $INSTALL_DIR/update ]; then
  cd $INSTALL_DIR
  source update
fi
EOF

svs_print_green "[+] Generando ${AUTOSTART}."
cat << EOF > $AUTOSTART
[Desktop Entry]
Encoding=UTF-8
Version=2.0
Type=Application
Name=Social Video Screen
Comment=Social Video Screen Entry
Exec=sudo $AUTORUN
OnlyShowIn=XFCE;
StartupNotify=false
Terminal=false
NoDisplay=true
EOF

chown "${SVS_USER}:${SVS_USER}" "${AUTORUN}"
chmod +x "${AUTORUN}"
}

### PERSISTENCE ###
svs_generate_rsa_persistence () {

  svs_print_green "[*] Generando las llaves RSA para el tunel SSH."

  local UUID
  local ID_RSA_PUB

  ID_RSA_PUB=$ROOT/.ssh/id_rsa.pub

  UUID=$(sudo dmidecode | grep "UUID"| cut -d ' ' -f 2)
  su - $SVS_USER -c "ssh-keygen -b 2048 -C ${UUID}@device.${SVS_USER}.com" <> /dev/tty 2>&1

  scp -P $PORT $ID_RSA_PUB $SERVER_USER@$SERVER:/home/socialscreen/.rsa_pending_keys.pub/${UUID}.pub

  echo "VALIDAR LAS LLAVES RSA EN EL SERVIDOR ${SERVER}"
}

svs_create_tunnel_ssh () {
cat << EOF > $CRONTAB
# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
SHELL=/bin/bash
# m h  dom mon dow   command
*/5 * * * * cat $PERSISTENCE | sudo -E bash -
0 3 * * * cat $DROP_CACHES_FILE | sudo -E bash -
EOF

  if [ -f $CRONTAB ]; then
    sudo crontab $CRONTAB && rm $CRONTAB
    svs_print_green "[+] Tunel SSH instalado."
  else
    svs_print_red "[-] Error al instalar el tunel SSH."
  fi
}

####

svs_create_auto_reload () {
cat << EOF > $CRONTAB_AUTO_RELOAD
# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
SHELL=/bin/bash
# m h  dom mon dow   command
@daily export XAUTHORITY=$ROOT/.Xauthority; export DISPLAY=:0; sudo $AUTORUN
EOF

  if [ -f $CRONTAB_AUTO_RELOAD ]; then
    sudo crontab -u $SVS_USER $CRONTAB_AUTO_RELOAD && rm $CRONTAB_AUTO_RELOAD
    svs_print_green "[+] Auto recarga de SVS programada todos los días a media noche."
  else
    svs_print_red "[-] Error al programar la auto recarga de SVS."
  fi
}

svs_main() {

  # Limpia la consola.
  reset || clear

  svs_print_orange "[*] Se requiere la instalacion de dependencias."
  svs_print_green "[*] Iniciando instalación de ${GIT_APP}."

  # Sobreescribir la configuración por defecto.
  svs_default_vars

  # RSA en git para continuar.
  svs_generate_rsa_git

  # Configura los directorios de instalación.
  svs_init

  # Clona la aplicación de git.
  svs_install

  # Instala las dependencias de la aplicación utilizando npm.
  svs_npm_install_deps

  # Genera las llaves RSA para conectarse con el servidor.
  svs_generate_keys_rsa

  # Establece el fondo y el splash de SVS.
  svs_set_background_splash

  # Configura las interfaces de red para conectarse al dispositivo.
  svs_configure_network_interface

  # Iniciala la aplicación cada vez que arranca el S.O.
  svs_generate_autorun

  # Crea el tunel SSH de forma persistente.
  svs_create_tunnel_ssh

  # Genera las llaves RSA para poder abrir el tunel SSH.
  svs_generate_rsa_persistence

  # Recarga el SVS todos los días a media noche.
  svs_create_auto_reload
  #
  svs_print_green "[*] Instalación finalizada."
}

svs_main

unset svs_source \
      svs_branch \
      svs_print \
      svs_print_red \
      svs_print_green \
      svs_print_blue \
      svs_print_orange \
      svs_default_vars \
      svs_generate_rsa_git \
      svs_init svs_install \
      svs_npm_install_deps \
      svs_generate_keys_rsa \
      svs_set_background_splash \
      svs_configure_network_interface \
      svs_generate_autorun \
      svs_generate_rsa_persistence \
      svs_create_tunnel_ssh \
      svs_create_auto_reload \
      svs_main
}